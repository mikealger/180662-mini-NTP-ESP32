#include "displays.h"
/**************************************************************************************************
 *    Function      : drawWifi
 *    Description   : draws the wifi sprite to tell the user to setup wifi
 *    Input         : ptr to active screen 
 *    Output        : none
 *    Remarks       : none
 **************************************************************************************************/
void drawWifi(U8G2_SSD1306_128X64_NONAME_F_HW_I2C *oled_ptr){

  oled_ptr->setFont(u8g2_font_open_iconic_all_8x_t);
  oled_ptr->drawGlyph(0,64,247);
  oled_ptr->setFont(u8g2_font_inb16_mf); 
  oled_ptr->drawStr(58,16,"Setup");
  oled_ptr->drawStr(70,48,"WiFi");
}

/**************************************************************************************************
 *    Function      : drawSplash
 *    Description   : draws the splash screen on boot
 *    Input         : ptr to active screen 
 *    Output        : none
 *    Remarks       : none
 **************************************************************************************************/
void drawSplash(U8G2_SSD1306_128X64_NONAME_F_HW_I2C *oled_ptr){

  oled_ptr->clearBuffer(); 
  oled_ptr->setFont(u8g2_font_open_iconic_all_8x_t);
  oled_ptr->drawGlyph(32,64,269);
}

/**************************************************************************************************
 *    Function      : drawNTPdetailsandGMTtime
 *    Description   : draws a screen showing the detials of the ntp server connection and the GMT time reported by the device
 *    Input         : ptr to active screen , utc time, and if pps is active 
 *    Output        : none
 *    Remarks       : none
 **************************************************************************************************/
void drawNTPdetailsandGMTtime(U8G2_SSD1306_128X64_NONAME_F_HW_I2C *oled_ptr,  datum_t utc_time,bool pps_active){
   char timestr[9]={0,};
     char datestr[16]={0,};
   
    oled_ptr->clearBuffer();
      oled_ptr->setFont(u8g2_font_amstrad_cpc_extended_8f );
      oled_ptr->drawStr(0,8,"NTP Server ");
      oled_ptr->drawStr(0,16,WiFi.localIP().toString().c_str());
      /* Determine if we are AP or STA */
      wifi_mode_t op_mode;
      if(ESP_OK == esp_wifi_get_mode( &op_mode) ){
        if(WIFI_MODE_STA == op_mode){
          oled_ptr->setFont(u8g2_font_open_iconic_all_1x_t);
          oled_ptr->drawGlyph(120,8,247);
          
        } else {
          oled_ptr->setFont(u8g2_font_open_iconic_all_1x_t);
          oled_ptr->drawGlyph(120,8,125);
          
        }
      } else {
        /* Error collecting WiFi Mode */
          oled_ptr->setFont(u8g2_font_open_iconic_all_1x_t);
          oled_ptr->drawGlyph(120,8,121);
      }
      oled_ptr->setFont(u8g2_font_inb16_mn ); 
      snprintf(timestr, sizeof(timestr),"%02d:%02d:%02d",utc_time.hour,utc_time.minute,utc_time.second);
      oled_ptr->drawStr(8,42,timestr);
      oled_ptr->setFont(u8g2_font_amstrad_cpc_extended_8f );
      snprintf(datestr, sizeof(datestr),"%04d-%02d-%02d GMT",utc_time.year,utc_time.month,utc_time.day);
      oled_ptr->drawStr(16,58,datestr);
      oled_ptr->setFont(u8g2_font_open_iconic_all_1x_t);
      if(true==pps_active){
        oled_ptr->drawGlyph(0,58,197);
      } else {
        oled_ptr->drawGlyph(0,58,123);
      }
}

/**************************************************************************************************
 *    Function      : drawWifi
 *    Description   : draws a screen showing the GPS position and local time.. 
 *    Input         : ptr to active screen, gpslocation valid, gps location lat and long local time and pps active 
 *    Output        : none
 *    Remarks       : none
 **************************************************************************************************/
void drawGPSposandLOCtime(U8G2_SSD1306_128X64_NONAME_F_HW_I2C *oled_ptr, bool gpslocationisValid,  double gpslocationlng, double gpslocationlat,datum_t loc_time,bool pps_active){
    char loc_timestr[9]={0,};
    char gps_lat_str[16]={0,};
    char gps_lng_str[16]={0,};
    char loc_datestr[16]={0,};
    
    
    
    oled_ptr->clearBuffer();
      if (true ==  gpslocationisValid ){
        oled_ptr->setFont(u8g2_font_amstrad_cpc_extended_8f );
        snprintf(gps_lng_str,sizeof(gps_lng_str),"Lng: %0.6f",gpslocationlng);
        snprintf(gps_lat_str,sizeof(gps_lat_str),"Lat: %0.6f",gpslocationlat);
        
        oled_ptr->drawStr(0,8,gps_lat_str);
        oled_ptr->drawStr(0,16,gps_lng_str);
        oled_ptr->setFont(u8g2_font_open_iconic_all_2x_t);
        oled_ptr->drawGlyph(16,112,209);
      } else {
         oled_ptr->setFont(u8g2_font_amstrad_cpc_extended_8f );
         oled_ptr->drawStr(24,16,"No GPS Fix");
      }
    
      
      oled_ptr->setFont(u8g2_font_inb16_mn ); 
      snprintf(loc_timestr, sizeof(loc_timestr),"%02d:%02d:%02d",loc_time.hour,loc_time.minute,loc_time.second);
      oled_ptr->drawStr(8,42,loc_timestr);
      oled_ptr->setFont(u8g2_font_amstrad_cpc_extended_8f );
      snprintf(loc_datestr, sizeof(loc_datestr),"%04d-%02d-%02d",loc_time.year,loc_time.month,loc_time.day);
      oled_ptr->drawStr(16,58,loc_datestr);
      oled_ptr->setFont(u8g2_font_open_iconic_all_1x_t);
      oled_ptr->drawGlyph(104,58,184);
      oled_ptr->setFont(u8g2_font_open_iconic_all_1x_t);
      if(true==pps_active){
        oled_ptr->drawGlyph(0,58,197);
      } else {
        oled_ptr->drawGlyph(0,58,123);
      }
}