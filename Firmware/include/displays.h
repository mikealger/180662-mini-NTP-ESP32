#include <U8g2lib.h>      // for all the oled ptr stuff and buffers
#include <WiFi.h>          // for WiFi functions
// #include <WiFiClient.h>
#include <esp_wifi.h>      // for esp specific wifi functions
#include "timecore.h"      // for time structure

void drawWifi(U8G2_SSD1306_128X64_NONAME_F_HW_I2C *oled_ptr);

void drawSplash(U8G2_SSD1306_128X64_NONAME_F_HW_I2C *oled_ptr);

void drawNTPdetailsandGMTtime(  U8G2_SSD1306_128X64_NONAME_F_HW_I2C *oled_ptr, 
                                datum_t utc_time,
                                bool pps_active);

void drawGPSposandLOCtime(  U8G2_SSD1306_128X64_NONAME_F_HW_I2C *oled_ptr, 
                            bool gpslocationisValid,  
                            double gpslocationlng, 
                            double gpslocationlat,
                            datum_t loc_time,
                            bool pps_active);

